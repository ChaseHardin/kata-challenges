﻿using System;

namespace Red_Pencil_Kata.Models
{
    public class Product
    {
        public decimal OriginalPrice { get; set; }
        public decimal SalePrice { get; set; }
        public DateTime LastDateModified { get; set; }
        public DateTime PromotionStartDate { get; set; }
        public DateTime PromotionEndDate { get; set; }
    }
}
