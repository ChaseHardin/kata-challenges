﻿using System;

namespace Red_Pencil_Kata.Services
{
    public interface IRedPencilService
    {
        double CalculatePercentage(decimal originalPrice, decimal salePrice);
        bool IsValidSalePercentage(double percent);
        bool IsDateStable(DateTime lastModifiedDate);
        bool IsValidPromotionPeriod(DateTime startDate, DateTime endDate);
    }

    public class RedPencilService : IRedPencilService
    {
        private const double MaximumPercentage = .30;
        private const double MinimumPercentage = .05;
        private const int MinimumStableDays = 30;
        private const int MaximumPromotionPeriod = 30;
        
        public double CalculatePercentage(decimal originalPrice, decimal salePrice)
        {
            return (double)((originalPrice - salePrice) / originalPrice);
        }

        public bool IsValidSalePercentage(double percent)
        {
            return percent <= MaximumPercentage && percent >= MinimumPercentage;
        }

        public bool IsDateStable(DateTime lastModifiedDate)
        {
            return DateDifference(DateTime.Now, lastModifiedDate) > MinimumStableDays;
        }

        public bool IsValidPromotionPeriod(DateTime startDate, DateTime endDate)
        {
            return DateDifference(endDate, startDate) <= MaximumPromotionPeriod;
        }

        private static double DateDifference(DateTime firstDate, DateTime secondDate)
        {
            return (firstDate - secondDate).TotalDays;
        }
    }
}