﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using Red_Pencil_Kata.Controllers;
using Red_Pencil_Kata.Models;
using Red_Pencil_Kata.Services;

namespace RedPencilKata.Tests.ControllerTests
{
    [TestFixture]
    public class RedPencilControllerTests
    {
        Mock<IRedPencilService> _redPencilServiceMock;
        private RedPencilController _redPencilController;

        [SetUp]
        public void SetUp()
        {
            _redPencilServiceMock = new Mock<IRedPencilService>();
            _redPencilController = new RedPencilController(_redPencilServiceMock.Object);
        }

        [Test]
        public void ShouldCreateRedPencilPromotion()
        {
            var percent = .2;
            var product = new Product
            {
                OriginalPrice = 100M,
                SalePrice = 80M, 
                LastDateModified = DateTime.Parse("01/25/16"), 
                PromotionStartDate = DateTime.Parse("03/01/16"),
                PromotionEndDate = DateTime.Parse("03/30/16")
            };

            _redPencilServiceMock.Setup(x => x.CalculatePercentage(product.OriginalPrice, product.SalePrice)).Returns(percent);
            _redPencilServiceMock.Setup(x => x.IsValidSalePercentage(percent)).Returns(true);
            _redPencilServiceMock.Setup(x => x.IsDateStable(product.LastDateModified)).Returns(true);
            _redPencilServiceMock.Setup(x => x.IsValidPromotionPeriod(product.PromotionStartDate, product.PromotionEndDate)).Returns(true);
            
            _redPencilController.CreateRedPencilPromotion(product);

            _redPencilServiceMock.Verify(x => x.CalculatePercentage(product.OriginalPrice, product.SalePrice), Times.Once());
            _redPencilServiceMock.Verify(x => x.IsValidSalePercentage(percent), Times.Once);
            _redPencilServiceMock.Verify(x => x.IsDateStable(product.LastDateModified), Times.Once);
            _redPencilServiceMock.Verify(x => x.IsValidPromotionPeriod(product.PromotionStartDate, product.PromotionEndDate), Times.Once);
        }
    }
}
