﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Red_Pencil_Kata.Services;

namespace RedPencilKata.Tests.ServiceTests
{
    [TestClass]
    public class RedPencilServiceTests
    {
        private readonly RedPencilService _service = new RedPencilService();

        [TestMethod]
        public void CalculatePercentage_TakesSalePrice_ReturnsFivePercentage()
        {
            // Arrange
            const int originalPrice = 100;
            const int salePrice = 95;

            // Act
            var result = _service.CalculatePercentage(originalPrice, salePrice);

            // Assert
            Assert.AreEqual(result, .05);
        }

        [TestMethod]
        public void CalculatePercentage_TakesSalePrice_ReturnsPercentage()
        {
            // Arrange
            const int originalPrice = 100;
            const int salePrice = 90;

            // Act
            var result = _service.CalculatePercentage(originalPrice, salePrice);

            // Assert
            Assert.AreEqual(result, .10);
        }

        [TestMethod]
        public void IsValidSalePercentageSale_TakesInPrices_ReturnsTrue()
        {
            // Arrange
            const double tenPercent = .10;

            // Act
            var result = _service.IsValidSalePercentage(tenPercent);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsValidSalePercentageSale_TakesPercentageOverThirtyPercent_ReturnsFalse()
        {
            // Arrange
            const double greaterThanThirtyPercent = .31;

            // Act
            var result = _service.IsValidSalePercentage(greaterThanThirtyPercent);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsValidSalePercentageSale_TakesPercentageLessThanFivePercent_ReturnsTrue()
        {
            // Arrange
            const double lessThanFivePercent = .04;

            // Act
            var result = _service.IsValidSalePercentage(lessThanFivePercent);

            // Assert
            Assert.IsFalse(result);

        }

        #region IsDateStable

        [TestMethod]
        public void IsStable_LastModifiedDateGreaterThanThirty_ReturnsTrue()
        {
            //Arrange
            var lastModifiedDate = DateTime.Now.AddDays(-30);

            //Act
            var result = _service.IsDateStable(lastModifiedDate);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsStable_LastModifiedDateLessThanThirty_ReturnsFalse()
        {
            //Arrange
            var lastModifiedDate = DateTime.Now.AddDays(-29);

            //Act
            var result = _service.IsDateStable(lastModifiedDate);

            //Assert
            Assert.IsFalse(result);
        }

        #endregion

        #region IsValidPromotionPeriod
        [TestMethod]
        public void IsValidPromotionPeriod_ThirtyDayPromotion_ReturnsTrue()
        {
            //Arrange
            var startDate = DateTime.Now.AddDays(5);
            var endDate = DateTime.Now.AddDays(35);

            //Act
            var result = _service.IsValidPromotionPeriod(startDate, endDate);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsValidPromotionPeriod_GreaterThanThirtyDays_ReturnsFalse()
        {
            //Arrange
            var startDate = DateTime.Now.AddDays(5);
            var endDate = DateTime.Now.AddDays(45);

            //Act
            var result = _service.IsValidPromotionPeriod(startDate, endDate);

            //Assert
            Assert.IsFalse(result);
        }
        #endregion
    }
}